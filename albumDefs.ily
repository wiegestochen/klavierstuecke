
rit      = { \override TextSpanner.bound-details.left.text = \markup { \bold\upright "rit." } } 
pocorit  = { \override TextSpanner.bound-details.left.text = \markup { \bold "poco rit." } }
acc      = { \override TextSpanner.bound-details.left.text = \markup { \bold\upright "acc." } } 
pedadlib = \markup{ \musicglyph "pedal.Ped" \italic "ad lib." }
simile   = \markup{ \italic "sim." }

caesura = \once \override BreathingSign #'text = #(make-musicglyph-markup "scripts.caesura.straight")

