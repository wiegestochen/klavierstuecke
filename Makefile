
# determine how many processors are present
CPU_CORES=`cat /proc/cpuinfo | grep -m1 "cpu cores" | sed s/".*: "//`

# The command to run lilypond
LILY_CMD = lilypond -ddelete-intermediate-files -dno-point-and-click -djob-count=$(CPU_CORES) -I Copyright

AUDIO_CMD = pianoteq --midi $< --mp3 $@

# The suffixes used in this Makefile.
.SUFFIXES: .ly .ily .pdf .midi .mp3

# Input and output files are searched in the directories listed in
# the VPATH variable. All of them are subdirectories of the current
# directory (given by the GNU make variable `CURDIR').
VPATH = \
  $(CURDIR)/Album \
  $(CURDIR)/PDF \
  $(CURDIR)/Copyright \
  $(CURDIR)/Blaetter \
  $(CURDIR)/Notes \
  $(CURDIR)/MIDI \
  $(CURDIR)/MP3

# The pattern rule to create PDF and MIDI files from a LY input file.
# The .pdf output files are put into the `PDF' subdirectory, and the
# .midi files go into the `MIDI' subdirectory.
%.pdf %.midi: %.ly
	$(LILY_CMD) $<; \
	mkdir -p PDF; \
	if test -f "$*.pdf"; then \
		mv -f "$*.pdf" PDF/; \
	fi; \
	mkdir -p MIDI; \
	if test -f "$*.midi"; then \
		mv -f "$*.midi" MIDI/; \
	fi

%.mp3: %.midi
	$(AUDIO_CMD); \
	mkdir -p MP3; \
	if test -f "$*.mp3"; then \
		mv -f "$*.mp3" MP3/; \
	fi

o    = album
i    = klassisch
ii   = weihnachtsgeschenk
iii  = tieferbassschluessel
iv   = dracherl
v    = augenblicke
vi   = vorahnung
vii  = winter
viii = never_ending_story
ix   = unruh
x    = abschied

notes = $(i).ily $(ii).ily $(iii).ily $(iv).ily $(v).ily $(vi).ily $(vii).ily $(viii).ily $(ix).ily

# The dependencies of the albumblaetter.
$(i).pdf: $(i).ly $(i).ily by.eps
$(ii).pdf: $(ii).ly $(ii).ily by.eps
$(iii).pdf: $(iii).ly $(iii).ily by.eps
$(iv).pdf: $(iv).ly $(iv).ily by.eps
$(v).pdf: $(v).ly $(v).ily by.eps
$(vi).pdf: $(vi).ly $(vi).ily by.eps
$(vii).pdf: $(vii).ly $(vii).ily by.eps
$(viii).pdf: $(viii).ly $(viii).ily by.eps
$(ix).pdf: $(ix).ly $(ix).ily by.eps
$(x).pdf: $(x).ly $(x).ily by.eps


# The dependencies of the full score.
$(o).pdf: $(o).ly $(notes)

# The dependencies of the mp3.
$(i).mp3: $(i).midi
$(ii).mp3: $(ii).midi
$(iii).mp3: $(iii).midi
$(iv).mp3: $(iv).midi
$(v).mp3: $(v).midi
$(vi).mp3: $(vi).midi
$(vii).mp3: $(vii).midi
$(viii).mp3: $(viii).midi
$(ix).mp3: $(ix).midi
$(x).mp3: $(x).midi


# Type `make album' to generate the full album of all 
# albumblaetter as one file.
.PHONY:   album
album:    $(o).pdf

# Type `make parts' to generate all parts.
# Type `make foo.pdf' to generate the part for instrument `foo'.
# Example: `make symphony-cello.pdf'.
.PHONY:   blaetter
blaetter: $(i).pdf \
          $(ii).pdf \
          $(iii).pdf \
          $(iv).pdf \
          $(v).pdf \
          $(vi).pdf \
          $(vii).pdf \
          $(viii).pdf \
          $(ix).pdf \
          $(x).pdf

.PHONY:   mp3
mp3:      $(i).mp3 \
          $(ii).mp3 \
          $(iii).mp3 \
          $(iv).mp3 \
          $(v).mp3 \
          $(vi).mp3 \
          $(vii).mp3 \
          $(viii).mp3 \
          $(ix).mp3 \
          $(x).mp3


all:      fresco album blaetter mp3

clean:
	find . -type f -name '*.midi' -execdir rm '{}' \;
	find . -type f -name '*.pdf' -execdir rm '{}' \;
	find . -type f -name '*.mp3' -execdir rm '{}' \;

fresco:
	find -type f -name '*.midi' ! -path "./MIDI/*" -execdir rm -f '{}' \;
	find -type f -name '*.pdf'  ! -path "./PDF/*"  -execdir rm -f '{}' \;

archive:
	tar -cvvf album.tar \   # this line begins with a tab
	--exclude=*pdf --exclude=*~ \
	--exclude=*midi --exclude=*.tar \
	../*

audio:  mp3


