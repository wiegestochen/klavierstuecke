#(ly:set-option 'relative-includes #t)
\version "2.22.0"
\language "deutsch"

\include "../albumDefs.ily"

date = #(strftime "%d.%m.%Y" (localtime (current-time)))

\paper {
  %print-all-headers = ##t
  #(set-paper-size "a4")
  #(define (print-positive-page-number layout props arg)
   (if (> (chain-assoc-get 'page:page-number props -1) 0)
       (create-page-number-stencil layout props arg)
       empty-stencil))
  #(define (not-last-page layout props arg)
     (if (and (chain-assoc-get 'page:is-bookpart-last-page props #f)
              (chain-assoc-get 'page:is-last-bookpart props #f))
         empty-stencil
         (interpret-markup layout props arg)))
  print-all-headers = ##f
  first-page-number = -1
  print-page-number = ##f
  tocItemMarkup = \markup { \large \fill-line { \override #'(line-width . 70) \fill-with-pattern #1 #CENTER . \fromproperty #'toc:text \fromproperty #'toc:page } } 
  %tocItemMarkup = \tocItemWithDotsMarkup
  tocTitleMarkup = \markup \huge \column {
    \fill-line { \null "Index" \null }
    \hspace #1
  }
  tocArtMarkup = \markup \large \column {
    \hspace #1
    \fill-line { \null \italic \fromproperty #'toc:text \null }
    \hspace #1
  }
  % offset the left padding, also add 1mm as lilypond creates cropped
  % images with a little space on the right
  %line-width = #(- line-width (* mm  3.000000) (* mm 1))
}

tocArt =
#(define-music-function (parser location text) (markup?)
   (add-toc-item! 'tocArtMarkup text))

\header {
  %{dedication = \markup {
    \lower #50 \left-align \center-column { 
      \fontsize #2 \bold "Hendrik Reupke"
    }
  %}
  title = \markup {
    \lower #50 \left-align \center-column { 
      \fontsize #4 \bold "Klavierstücke"
    }
  }
  subtitle = \markup {
    \lower #5 \left-align \center-column { 
      \fontsize #2 \bold "Hendrik Reupke"
    }
  }
  subsubtitle = \markup {
    \lower #4 \left-align \center-column { 
      \fontsize #1 \bold \with-url #"https://creativecommons.org/licenses/by-sa/3.0/de/" "(CC-BY-SA)"
    }
  }
  %copyright = \markup {© Hendrik Reupke \with-url #"https://creativecommons.org/licenses/by-sa/3.0/de/" "(CC-BY-SA)"}
  copyright = \markup {
    \fontsize #-3.5 {
      \override #'(box-padding . 1.0) \override #'(baseline-skip . 2.7) \box \center-column {
        \line {
          Gesetzt am \date mit \with-url #"http://lilypond.org/"
          \line {
            LilyPond \simple #(lilypond-version) (http://lilypond.org/)
          }
        }
      }
    }
  }
}

\pageBreak

\markuplist \table-of-contents
\markup \null
\tocArt \markup { Albumblatt }

\include "../Notes/klassisch.ily"

\bookpart {
  \paper {
    page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 1. Klassisch }
  \header { 
    title = \markup { \fromproperty #'toc:number "Albumblatt" }
    subtitle = "“Klassisch”"
    composer = "ca. 1994 + 2012"
    %composer = "Hendrik Reupke"
    subsubtitle = ##f
    copyright = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "1." }
    } <<
      \new Staff = "right" \right
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "left" { \clef bass \left }
    >>
    \layout { }
  }
}




\include "../Notes/weihnachtsgeschenk.ily"

\bookpart {
  \paper {
    %page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 2. Weihnachtsgeschenk }
  \header {
    dedication = "Für Ally"
    title = "Albumblatt"
    subtitle = "“Weihnachtsgeschenk”"
    composer = "2002"
    subsubtitle = ##f
    copyright = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "2." }
    } << 
      \new Staff = "up" { \key d \major \melody }
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "down" { \clef bass \key d \major \base }
      \new Dynamics \with { pedalSustainStyle = #'mixed } \pedal
    >>
    \layout {}
  }
}



\include "../Notes/tieferbassschluessel.ily"

\bookpart {
  \paper {
    %page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 3. Tiefer Bassschlüssel }
  \header {
    title = "Albumblatt"
    subtitle = "“Tiefer Bassschlüssel”"
    composer = "ca. 1997"
    subsubtitle = ##f
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "3." }
    } <<
      \new Staff = "right" { \clef bass \right }
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "left" { \left }
    >>
    \layout {
      \context {
        \Voice
        \consists "Melody_engraver"
        \override Stem #'neutral-direction = #'()
      }
    }
  }
}



\include "../Notes/dracherl.ily"

\bookpart {
  \paper {
    print-page-number = ##t
  }
  \tocItem \markup { 4. Liebeslied }
  \header {
    dedication = "Für mein Dracherl"
    title = "Albumblatt"
    subtitle = "“Liebeslied”"
    composer = "2006"
    %composer = "Hendrik Reupke"
    subsubtitle = ##f
    copyright = ##f
  }
  \score {
    \new PianoStaff \with {
        instrumentName = \markup { \fontsize #4 \bold "4." }
      } <<
        \new Staff = "right" \right
        \new Dynamics = "dynamics" \dynamics
        \new Staff = "left" { \clef "bass" \left }
      >>
    \layout {}
  }
}


\include "../Notes/augenblicke.ily"

\bookpart {
  \paper {
    print-page-number = ##t
  }
  \tocItem \markup { 5. Augenblicke von Dauer }
  \header {
    dedication = "Für Mutti"
    title = "Albumblatt"
    subtitle = "“Augenblicke von Dauer”"
    composer ="2010"
    subsubtitle = ##f
    copyright = ##f
    date = "Dezember 2010"
  }
  \score {
    \new PianoStaff \with { 
      instrumentName = \markup { \fontsize #4 \bold "5." }
    } <<
      \new Staff = "upper" \upper
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "lower" { \clef bass \lower }
    >>
  }
}



\include "../Notes/vorahnung.ily"

\bookpart {
  \paper {
    %page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 6. Unheil }
  \header {
    title = "Albumblatt"
    subtitle = "“Unheil”"
    composer = "2011"
    subsubtitle = ##f
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "6." }
    } <<
      \new Staff = "right" << \rightOne \\ \rightTwo >>
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "left" { \clef bass \left }
    >>
    \layout { }
  }
}




\include "../Notes/winter.ily"

\bookpart {
  \paper {
    page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 7. Winter }
  \header {
    title = "Albumblatt"
    subtitle = "Winter"
    subsubtitle = "Inspiriert von ''The First Noël''"
    composer = "Mai 2021"
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "7." }
    } <<
      \new Staff = "upper" \PartPOneVoiceOne
      \new Dynamics = "dynamics" \dynamics
      \new Staff = "lower" \PartPOneVoiceTwo
    >>
    \layout { }
  }
}






\include "../Notes/never_ending_story.ily"

\bookpart {
  \paper {
    %page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 8. Never Ending Story }
  \header {
    title = "Albumblatt"
    subtitle = "Never Ending Story"
    subsubtitle = "(\"So mal eben\" von Tanja Schmidt-Soltau)"
    composer = "Mai 2021"
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "8." }
    } <<
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceTwo" { \voiceTwo \PartPOneVoiceTwo }
      >>
      \new Dynamics = "dynamics" \dynamics
      \context Staff = "2" <<
        \context Voice = "PartPOneVoiceThree" { \voiceOne \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceFive" { \voiceTwo \PartPOneVoiceFive }
      >>
    >>
    \layout { }
  }
}



\include "../Notes/unruh.ily"

\bookpart {
  \paper {
    page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 9. Unruh }
  \header {
    title = "Albumblatt"
    subtitle = Unruh
    subsubtitle = "Tick - Tick - Tick"
    composer = "12. - 17. Juni 2021"
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "9." }
    } <<
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
      >> 
      \new Dynamics = "dynamics" \dynamics
      \context Staff = "2" <<
        \context Voice = "PartPOneVoiceTwo" { \PartPOneVoiceTwo }
      >>
    >>
    \layout { }
  }
}




\include "../Notes/abschied.ily"

\bookpart {
  \paper {
    page-count = #1
    print-page-number = ##t
  }
  \tocItem \markup { 10. Abschied }
  \header {
    dedication = "für Katrin General, zum Abschied"
    title = "Albumblatt"
    subtitle = "Abschied"
    subsubtitle = ##f
    composer = "10. – 12. März 2022"
    copyright = ##f
    tagline = ##f
  }
  \score {
    \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "10." }
    } <<
      \context Staff = "1" {
        \context Voice = "right" { \voiceOne \right }
      }
      \new Dynamics = "dynamics" \dynamicsLayout
      \context Staff = "2" {
        \context Voice = "left" { \clef "bass" \left }
      }
    >>
    \layout { }
  }
}
