
global = {
  \key c \major
  \time 2/4
  \tempo 4 = 100
}

right = \relative c' {
  \global \partial 8 
  d'8-1( |
  e2-2) | 
  r8 c( e8. a16 |
  g4 a,) |
  r4. f'8-3( |
  e2-2) |
  r8 c( e c' |
  h a g a |
  g2) |
  R2 |
  r8 c,( g' c) |
  a-2( h c d |
  f,4 h) |
  <c e,>2 |
  <a c,> |
  g4-5 g,16( c-2 e f) |
  g2~ |
  g8\breathe f8-4( d-2 g-5) |
  e2-3~ |
  e4 c16-1( e-2 g c) |
  g2~ |
  g4 f8. g16 |
  e16 h16~ h4 c8-5( |
  e,4.-2) c8( |
  g'2) |
  d4.\breathe g'8-1 |
  c-2( d-1 e-3 f-4) |
  g4.-5 \ottava 1 c,8-1 |
  a' g h, f' |
  g4. c,8-2 |
  g'-5 f a, d |
  e2 | \ottava 0
  h8-4 c e,8.-2 c16 |
  g'2-4 |
  f2 |
  f |
  g4.\fermata \bar "|."
}

%_\markup{ \italic "ad lib." }
left = \relative c {
  \global %\set Staff.pedalSustainStyle = #'mixed
  %\set Staff.pedalSustainStrings = #'( "Ped" "" "")
  r8 |
  c8--(_\pedadlib g' c h,) |
  a--( e' h' a) |
  f,--( c' f a) |
  g,--( d' h' g) |
  c,--( g' c h,) |
  a(--_\simile e' c' a) |
  f,( c' a' f) |
  g,( d' h' d) |
  c,( g' c d) |
  e,( c' g' e) |
  f,( c' g' f) |
  h,,( g' d' h) |
  a,( e' c' a) |
  f,( c' a' f) |
  e,( c' g' e) |
  g,( d' g c) |
  h2 |
  c,8( g' c h,) |
  a( e' c' a) |
  f,( c' f a) |
  g,( d' h' g) |
  a,( e' h' c) |
  g,( d' g4) |
  e,8( h' e g) |
  g,^(~ <g d'> ~ <g d' f>4) ~ |
  <g d' f>2 |
  c8( g' d' e) |
  f,( d' f h,) |
  e,( c' g' e) |
  f,( c' g' f) |
  c,( g' d' e) |
  a,,( e' h' c) |
  f,,( c' g' a) |
  g,( d' a' c) |
  c,( g' c d |
  e4.)
}

dynamics =
{

}
