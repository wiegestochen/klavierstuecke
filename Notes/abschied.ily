
\version "2.23.6"
\language "deutsch"

\include "articulate.ly"

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key c \major
  \time 4/4
  \tempo "Andante" 4=85
  \mergeDifferentlyHeadedOn
  \mergeDifferentlyDottedOn
  \override NoteColumn.ignore-collision = ##t
}

DCcoda = {
  %\once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  %\once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \once \override Score.RehearsalMark.break-visibility = #end-of-line-visible
  \once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup { \small "D.C. al coda" }
}

GotoCoda = {
  %\once \override Score.RehearsalMark #'break-visibility = #'#(#t #t #f)
  %\once \override Score.RehearsalMark.self-alignment-X = #RIGHT
  \mark \markup { \small { \musicglyph #"scripts.coda" } }
}

Coda = {
  %\once \override Score.RehearsalMark #'break-visibility = #'#(#f #t #t)
  \mark \markup { { \musicglyph #"scripts.coda" } }
}

rightCaputMelody = \relative c' {
  %\slurDown
  c4( h a2 |
  g g) |
  c4( h c2 |
  d^2 e^3)\GotoCoda |
}

rightCaputOrnament = \relative c'' {
  <>^\markup{ \italic "grazioso" } g4-3( <c g c,>) <a c, g>2 |
  <g c, f,> <g^5 d^3>4 ~ <g c,_2> |
  g4-3( <c g c,>) <a^5 e^3>2 |
  <g-4 c,> <g d> |
}

rightCaput = {
  <<
    \new Voice = "melody" { \voiceTwo \rightCaputMelody }
    \new Voice = "ornament" { \voiceOne \rightCaputOrnament }
  >>
}

rightCaputEndMelody = \relative c'' {
  %\phrasingSlurUp
  \voiceThree c4^4( e) \voiceTwo f,,2( |
  g2-2 f4-1 f |
  f2 e |
  <d' g,>1)^\fermata |
}

rightCaputEndOrnament = \relative c'' {
  \voiceFour <g c,>2 \voiceOne <d^5 g,^2>2 | 
  << { s2 g, } \\ e'1 >> |
  << { \voiceThree \once \hide c4( h) \once \hide c( a) } \\ { \voiceThree c2 c } >> |
  c2( h) |
}

rightCaputEnd = {
  <<
    \new Voice = "melody" { \voiceTwo \rightCaputEndMelody }
    \new Voice = "ornament" { \voiceOne \rightCaputEndOrnament }
  >>
}

rightInterludeMelody = \relative c' {
  f4-3( c') \breathe c( f,-3 |
  g-4 c d,4.-2) c8\( |
  e4( h') e,( a) |
  f-3( d') f,( e)\) |
  <d c a>1 |
  <c'^5 g^3>2( <h^4 f^2>) \DCcoda |
}

rightInterludeOrnament = \relative c' {
  c1 |
  <c d>1 |
  <e d h>2 <e c h> |
  <f d g,>2 <f c g> |
  { \voiceThree r4 r8 a( a4\finger \markup \tied-lyric #"5~3" ~ \tuplet 3/2 { a8 h c) } } |
  c,1 |
}

rightInterlude = {
  <<
    \new Voice = "melody" { \voiceOne \rightInterludeMelody }
    \new Voice = "ornament" { \voiceTwo \rightInterludeOrnament }
  >>
}

rightCodaMelody = \relative c'' {
  \Coda
  \voiceThree c4-4( e) \voiceTwo f,,2( |
  g-2 g4 f |
  
  %\voiceThree c'-4( h-3) c-4( a-2) \voiceTwo |
  e2 f |  
  
  <d' g,>1^\markup{ \italic "rubato" } |
  \voiceOne <d' g, d c>2( <g c, g f> | 
  <d' g, d c>1) |
}

rightCodaOrnament = \relative c'' {
  <g c,>2 \voiceOne <d^5 a^2> | 
  << { s2 a-3 } \\ e'1^5 >> |
  
  %\voiceFour <c e,>2 <c f,> |
  << { \voiceThree c2 c } \\ { \voiceThree \once \hide c4( h) \once \hide c( a) } >>
  
  c2^5 h^3 | 
}

rightCoda = {
  <<
    \new Voice = "melody" { \voiceOne \rightCodaMelody }
    \new Voice = "ornament" { \voiceTwo \rightCodaOrnament }
  >>
}

rightCodaEndeLayout = \relative c''' { <e g, e d>1\laissezVibrer\fermata | }
rightCodaEndeMidi = \relative c''' { <e g, e d>\longa ~ | <e g, e d>\breve }

right = \relative c'' {
  \global
  \rightCaput
  \rightCaputEnd
  \bar "||"
  \rightInterlude
  \bar "|." \break
  \cadenzaOn s64 \cadenzaOff
  \rightCoda
  \rightCodaEndeLayout
  \bar "|."
}

leftCaput = \relative c {
  <c c,>2( <f, f,> |
  <g g,> <e' c,>) |
  <c h c,>( <g' f f,> ~ |
  <g f g,> <c g c,>) |
}

leftCaputEnd = \relative c' {
  <f, a,>( <h, h,> |
  <c c,> <f, f,>) |
  <g g,>( <a a,> |
  <g g,>1) |
}

leftInterlude = \relative c {
  <d d,>1( |
  <e e,>) |
  <gis, gis,>2( <a a,> |
  <h h,> <c c,>) |
  <fis, fis,>1( |
  << \new Voice { g2. \breathe g4 } g,1) >> |
}

leftCoda = \relative c {
  <f a,>2( <h, h,> |
  <c c,> <f, f,>) |
  <g g,>( <a a,>) |
  <h h,>1\sustainOn ~ |
  <h h,> ~ |
  << <h h,> \new Voice { s2 g, } >> \sustainOn\sustainOff |
}

leftCodaEndeLayout = \relative c, { <c c,>1\laissezVibrer\fermata | }
leftCodaEndeMidi = \relative c, { <c c,>\longa ~ | <c c,>\breve | }

left = \relative c {
  \global
  \set Staff.pedalSustainStyle = #'mixed
  \leftCaput
  \leftCaputEnd
  \bar "||"
  \leftInterlude
  \bar "|." \break
  \cadenzaOn s64 \cadenzaOff
  \leftCoda 
  \leftCodaEndeLayout
}

dynamicsLayout = {
  s1*6 |
  s1\> |
  s2 s\p |
  s1\< |
  s1\! |
  s1\< |
  s1 |
  s1\! |
  s1 |
  s64 s1 |
  s1 |
  s1\> |
  s1 |
  s1\! |
}

dynamicsMidi = {
  s1*6 |
  s1\> |
  s2 s\p |
  s1\< |
  s1\! |
  s1\< |
  s1 |
  s1\! |
  s1 |
  %coda
  s1 |
  s1 |
  s1\> |
  s1 |
  s1\! |
}