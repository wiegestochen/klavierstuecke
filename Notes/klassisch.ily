
global = {
  \key c \major
  \time 4/4
}

right = \relative c' {
  \global
  \tempo "Vivace" 4=168
  c8( e) h( e) c( e) h( e) |
  c a'( f) a( e) a( d,) a' |
  c,8( e) h( e) c( e) h( e) |
  c a'( f) a( e) a( d,) gis |
  a( c g c f, ais g ais |
  a! g f a d, g e g) | 
  f4( a <d,~ a'> <d gis>) | \bar "||"
  
  c'4 \tuplet 3/2 { c8( d e) } << e2 \\ { h4 gis8^( h) } >> |
  c4 \tuplet 3/2 { c8( d e) } << e2 \\ { h4 gis8^( h) } >> |
  c( e d f e g g fis | <h, g'>1) |
  g'4 \tuplet 3/2 { g8( a h) } << { h4~ \tuplet 3/2 { h8 a( g) } } \\ e2 >> |
  f4 \tuplet 3/2 { f8( g a) } << { a4~ \tuplet 3/2 { a8 g( f) } } \\ d2 >> | 
  e4 << { \tuplet 3/2 { e8~ e( d) } c4 } \\ h2 >> << { \tuplet 3/2 { c8~ c( h) } | a4 } \\ g2 >> % | kein barcheck wegen der Halben
  << { \tuplet 3/2 { a8~ a( g) } f4 } \\ e2 >>
  << { f8( g) %\rit 
       e4\startTextSpan e8( d) c4 h\stopTextSpan } \\ { d4~ d4 a2 } >> |
  \tempo "a tempo" << { a'8( e) h'( f) c'( g) d'( g,) e'( c) g'( d) s4 e, } \\ { c4 d e f g h <gis h d gis>2^\fermata } >> | \bar "||"
  
  \tempo "Largo" 4=60
  <c, e>8( <h e> <c e> <f a> <e a> <d gis> <c e a>4) |
  << { c'8 h c <e, a c e> d'( h) a( <gis h>) } \\ { s2 f4 d } >> |
  <e a c>8( <a c e> <a c f>4) <h~ d g~>8( <h d g> <g h e>4) |
  \time 3/4 <a c e a>4-> <a c f a>-> << { a'8( h) } \\ <a, d f a>4^> >> |
  \time 2/4 <e' a h>4~ <e gis h>8 \breathe <h, e gis>( | \bar "||"
  
  \time 4/4 <c e a>8)-- e'( c a) <d, f gis>( a' h) <d, f gis>( |
  <c e a>)-- e'( c a) <f a d>( e' f d) | 
  <g, c e>--( g' f e) <f, h d>--( f' e d) |
  <e, a c>--( e' d c) <h, d gis>4 <c e a>\fermata | \bar "||"
  
  %\time 6/8 
  \tempo "Presto" 4=200 c8[( e] h[ e]) c2-. |
  \ottava #1
  c'8[( e] h[ e]) c2-. |
  \ottava #2
  c'8[\(( e] h[ e]) c4-. <d gis>4-. | 
  <c e a>1-.\) | \bar "|."
}

lgis = \relative c { <gis gis'>4 }
la = \relative c { <a a'>4 }
lh = \relative c { <h h'>4 }
lc = \relative c { <c c'>4 }
ld = \relative c { <d d'>4 }

left = \relative c {
  \global
  <a a'>4-.\( <gis gis'>-. <a a'>4-. <gis gis'>-. |
  <a a'>-.\) <d d'>-.\( <c c'>-. <h h'>-.\) |
  <a a'>4-.\( <gis gis'>-. <a a'>4-. <gis gis'>-. |
  <a a'>-.\) <d d'>-.\( <c c'>-. <h h'>-.\) |
  %\clef treble
  <f' f'>-. <e e'>-. <d d'>-. <e e'>-. | 
  <f f'>-. <d d'>-. <h h'>-. <cis cis'>-. | 
  <d a' d>2 <e h' e> | \bar "||"
  
  \clef treble 
  <a e' a>2 <gis e' gis> |
  <a e' a>2 <gis e' gis> |
  <a e' a>4 <h g' h> <c g' c> <d a' d> | <g, d' g>1 |
  << { e''4 e cis cis } \\ { <e, h'>2 <cis a'> } >> |
  << { d'4 d h h } \\ { <d, a'>2 <h g'> } >> |
  <c g' c>4 <gis d' gis> <a e' a> <e c' e> | \clef bass
  <f c' f> <cis a' cis> <d a' d> <h g' h> |
  <c g' c> <f, c' f> <g d' g>2 |
  <a e' a>4 <h g' h> <c g' c> <d g d'> | <e c' e> <f h f'> 
%  \ottava #-1
  <e,, e'>2 | \bar "||" 
%  \ottava #0
  
  <a' e' a>8 <gis e' gis> <a e' a> <d a' d> <c a' c> <h gis' h> <a e' a>4 |
  \clef treble <a' e' a>8 <gis e' gis> <a e' a> \clef bass <f c' f> <d a' d>4 <e h' e> |
  <a, e' a>4 <d a'_~ d>8 <c a' c> <h g' h>4 <c g'_~ c>8 <h g' h> |
  \time 3/4 <f f'>( <e e'> <d d'> <c c'> <h h'>4) | 
  \time 2/4 <e e'>4. <d d'>8 |
  
  \time 4/4 <c c'>2 <d d'>4. <d d'>8 |
  <e e'>2 <f f'> <g g'> <gis gis'> | 
  <a a'> <e e'>4 <a, a'> |
  
  %\time 6/8 
  <a' a'>4-. <gis gis'>-. <a a'>2-. |
  \ottava #1
  <a' a'>4-. <gis gis'>-. <a a'>2-. |
  \ottava #2
  <a' a'>4-. <gis gis'>-. <a a'>-. <d h'>-. |
  <a e' a>2-.
  \ottava #0
  <a,,, a'>2-. | \bar "|."
}

dynamics =
{
  s1\mf
  s1*6
  
  s1\mp
  s1
  s8\< s8 s2.
  s1\mf
  s1\mp
  s1*4
  s8\mf\< s2..
  s2 s2\f
  
  s1\p
  s1
  s1\<
  s2.\f
  s2
  
  s1\mf
  s1
  s1\>
  s2. s4\p
  
  s1\<
  s1*2
  s1\f
}