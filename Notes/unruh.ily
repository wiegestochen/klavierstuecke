
PartPOneVoiceOne =  \relative fis' {
  \clef "treble" \stopStaff \override Staff.StaffSymbol #'line-count =
  #5 \startStaff \key d \major \time 6/8 \repeat volta 2 {
    | % 1
    \tempo 4=80 fis8 ( fis fis fis fis fis g g g e e e )
    | % 3
    fis8 ( fis fis d' d d e e e cis cis cis ) | % 5
    d8 ( d d h h h | % 6
    a8 a a a a a ) a ( a a a a a a a a a a a ) \bar
    "||"
    e8 ( e e fis fis fis a a a a a a ) h ( h16 a h
    cis16 d8 d d d d d cis cis cis ) cis ( cis cis h h
    fis16 g \bar "|"
  }
  \alternative {
    {
      a8 a a a a g )
    }
    {
      a8 ( a a a a a
    }
  } \bar "|"
  a8 a a a a a a a a a4. ) \bar "||"
  \tempo 4=70 fis8 ( -3 fis16 g fis e d4 a'8 h cis d g,->
  fis8. ) \fermata a16 -5 h, g' cis, -2 g' d -2 fis -4 e
  a16 fis a a, a' h, g' cis, g' d g e4 g16 a fis8
  cis8 d g d e a a, g' fis16 e d8 <d' fis,> ~ <h fis>4 ~ ~ <h fis>16
  h16 a4 ~ a16 a ~ <d, a'>4 g8 e4 e8 | % 26
  \numericTimeSignature\time 4/4  a,4 <d fis,> g, ~ <g cis> <fis d'>1
  \bar "||"
  \time 6/8  | % 28
  \tempo 4=80 <fis' d'>8 ( <fis d'> <fis d'> <fis d'> <fis d'>
  <fis d'>8 |
  <e' g,> <g, e'> <g e'> <g cis> <g cis> <g cis> ) |
  <fis h>8 <fis h> <fis h> <d' fis,> <d fis,> <d fis,> |
  <g, g'> <g g'>8 <g g'> <cis g'> <cis g'> <cis g'> |
  <d fis> <d fis> <d fis>8 <h fis'> <h fis'> <h fis'> |
  <a e> <a e> <a e> <d, fis>8 <d fis> <d fis> |
  g g g fis fis fis |
  e e e e e e \bar "||" |
  <h g'>8 q q q q q | %36
  <e a> q q <e' g,> q q | 
  <fis, d'> q q <h g> q q |
  <d, a'> q q <d h'>4. |
  \once \override BreathingSign #'text = #(make-musicglyph-markup "scripts.caesura.straight")
  \breathe 
  g8 a h a d,4 | 
  h'8 cis d d8 a4 |
  <h d,>8 <cis, h'> <e a> <fis d>4 <fis d a>8 |
  <e g, a>4. <fis, d' a>4. \fermata \bar "|."
}

PartPOneVoiceThree =  \relative fis' {
  \clef "treble" \stopStaff \override Staff.StaffSymbol #'line-count =
  #5 \startStaff \key d \major \time 6/8 \repeat volta 2 {
    | % 1
    \tempo 4=80 s1. | % 3
    s1. | % 5
    s2. | % 6
    s1. fis2. \bar "||"
    s2. e4. fis d4 g8 fis4. g e d2. \bar "|"
  }
  \alternative {
    {
      <d e>4. ~ <cis e>
    }
    {
      <d e>4. ~ <cis e>
    }
  } \bar "|"
  <d fis>2. ~ ~ <d fis> \bar "||"
  \tempo 4=70 s4*15 r2 d4 ~ d4. cis s2. | % 26
  \numericTimeSignature\time 4/4  s1*2 \bar "||"
  \time 6/8  | % 28
  \tempo 4=80 s4*21 d4. cis \bar "||"
  s1*6 \bar "|."
}

PartPOneVoiceTwo =  \relative d' {
  \clef "bass" \stopStaff \override Staff.StaffSymbol #'line-count =
  #5 \startStaff \key d \major \time 6/8 \repeat volta 2 {
    d4. ( h ) e ( cis ) | % 3
    d4 ( ~ d16 cis h4. ) g ~ <g a> ~ | % 5
    <h g>4 ~ h16 ~ <fis h> ~ <g h>4. ~ | % 6
    <cis e, g>4. <d fis,>4 ~ ~ <fis, d'>16 g <a d>4. ~ <cis a e>
    ~ <a d d,>2. \bar "||"
    <cis, a' cis>4. ~ <d' a d,> <cis, a' cis> ~ <d' a d,> <g,,
    g'>4 <e e'>8 <h'' h,>4. <e,, e'> <a a'> <fis' fis,> <g
    g,>4. \bar "|"
  }
  \alternative {
    {
      <a a,>2.
    }
    {
      <a a,>2.
    }
  } \bar "|"
  <d, d,>2. ~ ~ <d d,> \bar "||"
  d'8 ( -1 a -3 cis -1 h -2 fis d' g, e' fis a,-> d )
  \fermata \breathe fis,8 g e fis <cis a'> d fis g, a' h,_3 cis
  a8 cis d4. h cis4 a8 d a'4 g8 fis4 a8 g4 h,8 a'4 cis,8 g'4 | % 26
  \numericTimeSignature\time 4/4  fis8 a, ~ a d e a, a'4 <d, d,>1
  \bar "||"
  \time 6/8  d'4.( h) |
  e( a,) |
  d4 ~ d16 cis h4 ~ h16 d |
  e4. a,4. |
  h4 ~ h16 fis g4 ~ g16 d' |
  fis,4. h |
  e8( cis a) d( a fis) |
  a4. ~ <a, a'>4. \bar "||"
  <e' g>2. cis4. a h e fis g |
  e( fis4) c'8 |
  g4.( fis4) d'8 |
  g, e fis8 h fis h, |
  e a, cis <d, d'>4. \bar "|."
}

dynamics =
{

}
