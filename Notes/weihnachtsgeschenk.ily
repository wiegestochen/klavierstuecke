

melody = \relative c'' {
  \time 6/8 
  \tempo "fokussiert" 4=120
  s4. a8( d g |
  fis8) g,( a) d( cis e, |
  fis) e( fis) a-3( cis d-1 |
  e fis g a h cis | 
  d) d,( fis) d'( cis d | 
  cis) cis,( e) h'( a h |
  a) h,( d) a'( g fis | 
  e d cis h a g | 
  
  fis4) \caesura \breathe s8 a8( d g8 |
  fis8) g,( a) d( cis e, |
  fis) e( fis) a( cis d |
  e fis g a e' cis | 
  d) d,( fis) h( d h | 
  cis) cis,( e) a( cis a | 
  h) h,( d) h'( a g | 
  \rit fis\startTextSpan e d cis h4\stopTextSpan) \bar "||" | 
  
  \break
  
  \tempo "verträumt" 4=100
  a4( d fis |
  g4) s8 g,8( h fis' |
  e4) s8 e,8( g cis |
  d4) s8 d,8( fis d' |
  cis4) s8 cis,8( e a |
  h4) s8 h,8( d h' |
  a4) s8 a,8( d fis |
  g4) s8 h,8( d g |
  e4) \caesura \breathe s8 a( d fis |
  <h, g'>4) s8 e( <cis a'> e |
  <a, fis'>4) s8 fis'( h cis |
  <d fis,>4) <cis e,> <a cis,> | 
  <fis a,> s8 a,( d fis | 
  <cis' e,>4) <h d,> <a cis,> | 
  <d, a>2 d4( | 
  <a' cis, a>4) r8 fis( e d ~ | 
  <e d a>2.) \bar "||" |
  
  \pageBreak
  
  \tempo "rastlos" 4=130
  r8 a,( d g <fis g,> d |
  g, <g e>4 cis <d, fis d'>8) ~ |
  <d fis d'> d( fis d' <cis e,> a | 
  e <d a'>4 d8 cis <a d>) ~ |
  <a d> a( d a' <a, d a'> g' | 
  fis <d a>4 a g8) ~ |
  g g( d' g g fis |
  e d4 g,8 cis d) ~ |
  d \caesura \breathe a'( d g <fis g,> d |
  g, a'4 <e cis'> <fis d'>8) ~ |
  <fis d'> <e cis'>4( <cis a'> <a fis'>8) ~ |
  <a fis'>4 <h g'>8( <a fis'>4 <fis d'>8 ~ |
  <fis d'> <d a'>4.) <h g'>8( <a fis'>8 ~ |
  \pocorit <a fis'> \startTextSpan <g d' e>4 g8 cis4 \stopTextSpan |
  <cis d>2. |
  <a h>2.) \bar "||" | 
  
  %\break
  
  \tempo "sinnend" 4=110
  d4 ~ <cis d fis a>2 ~ | 
  <cis d fis a>4 h4 ~ <a h d>4 ~ |  
  <a h d>2.
  cis16(\( d) fis16( a) cis( d) fis( a) 
  \ottava #1 cis( d) fis( a) |
  cis2.\) \bar "|."
}

base = \relative c {
  %\shiftOff
  \time 6/8 
  d8( a' d) s4 s8 |
  d,4( a' d) | % d,4 ~ <d a'> ~ <d a' d> |
  d,( a' d) | % d4 ~ <d a'> ~ <d a' d> |
  cis,( a' cis) | % cis, cis4 ~ <cis a'> ~ <cis a' cis> |
  h,( fis' h) | 
  a,( fis' a) | 
  g,( d' g) |
  a,( e' a) |
  
  d,8( a' d) s4. |
  d,4( a' d) | 
  d,( a' d) | 
  cis,( a' cis) | 
  h,( fis' h) | 
  a,( fis' a) | 
  g,( d' g) |
  a,( e' a) |
  
  
  R2. |
  d,8( a' d) s4. |
  cis,8( a' cis) s4. |
  h,8( fis' h) s4. | 
  a,8( fis' a) s4. | 
  g,8( d' g) s4. |
  fis,8( d' fis) s4. |
  e,8( h' e) s4. |
  a,8( e' a) s4. |
  
  d,8( a' d) s4. |
  d,8( a' d) s cis,4 |
  h8( fis'4 h fis8) |
  h,4( fis' h) | 
  a,8( e'4 a e8) | 
  g,4( d' g) | 
  fis,( cis' fis) |
  a,( e' a) |
  
  
  d,4( a' d) | 
  r <cis, cis'>( a') |
  <h, h'>( fis' h) | 
  r <a, a'>( e') |
  <g, g'>( d' g) | 
  r <fis, fis'>( d') |
  <e, e'>2(  h'4) | %h'
  r <a, a'>2 | %e') |
  
  d'4( a' d) |
  r <cis, cis'>( a') |
  <h, h'>( fis') <a, a'>( | 
  fis') <g, g'>( d') |
  <fis, fis'>( d') <e, e'>( |
  h') <a, a'>( e') |
  <h' fis' h>2. |
  <g d' g>2. | 
  r4 <h fis' h>2 ~ |
  <h fis' h>2 <g d' g>4 ~ |
  <g d' g>2. ~ |
  <g d' g>2. ~ |
  <g d' g>2. |
}

pedal = { 
  s2.\sustainOn |
  s2.\sustainOff\sustainOn |
  s2.\sustainOff_\markup{\italic sim.} |
}

pedal_midi = {
  s2.\sustainOn |
  \repeat unfold 52 { s8\sustainOff s8\sustainOn s2 } |
  s2.\sustainOff | % 54
}

dynamics =
{

}
