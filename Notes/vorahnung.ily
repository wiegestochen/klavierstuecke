
global = {
  \key c \major
  \numericTimeSignature
  \time 2/4
  \tempo 4=100
}

rightOne = \relative c' {
  \global
  \shape #'((0 . 1) (0 . 0) (0 . 0) (0 . 1)) PhrasingSlur
  \tieDown <c e>4\pp\( <c e>8. <c~ e^~>16 <c e>4 <c e>8 <c~ g'^~> <c g'>4 <c g'>8 <c~ d^~> <c d>4 <c d>\) | \break
  \shape #'((0 . 0) (0 . 3) (0 . 1) (0 . 0)) PhrasingSlur
  <c e>4\p\<\( <c e>8. <c~ e^~>16 <c e>4 <c e>8 <c~ a'^~> <c a'>4 <c a'>8 <d~ h'^~> <d h'>4 <d h'>8 <e~ c'^~>\) \break
  \repeat volta 2 {
    <e c'>4 c' | <f, c'> c' | <g c> c | <a c> <a c> | \break
  }
  \repeat volta 2 {
    g2\mf 
    \shape #'((0 . 0) (0 . 0.5) (0 . 3) (0 . 0.5)) PhrasingSlur
    g\( | \stemDown g,8 a ~ a4\) \stemUp \tieUp c2\( | e8 f ~ f4\) e8\( f e d | 
  }
  \alternative {
    { e f ~ f4\) f8 << { g ~ g4 } \\ { \stemUp s4 d8 } >> | }
    { e8\< f e d e f e d | }
  }
  <e c'>4 <f c'> <g c> <a c> | <c e c'>1\ff
}

rightTwo = \relative c' {
  \global
  s2*8 | 
  \repeat volta 2 {
  s4 e8. e16 | s4 f8. f16 | s4 g8. g16 | s2 |
  }
  \repeat volta 2 {
    d8 e ~ e4 d8 e d c | \stemUp c2 \stemDown g8 a c e | c2 c | 
  }
  \alternative {
    { c c4 c | }
    { \stemDown c2 c | }
  }
}

left = \relative c' {
  \global
  a8\pp h a h | g h g h | f g f g | g a g a |
  a8 h a h | g h g h | f g f g | e gis e gis |
  \repeat volta 2 {
    a h a h | c d c d | d e d e | f g f g |
  }
  \time 4/4
  \repeat volta 2 {
    << 
      { c,4 \tieDown g ~ g2 | f4 c ~ c2 | d4 a ~ a2 } \\ 
      { c2 ~ c | f, ~ f | d ~ d | } \\ 
      { \shiftOff s4. c''8 ~ c2 | s4. f,8 ~ f2 | s4. d8 ~ d2 } >>
  } 
  \alternative {
    { << 
      { g4 \tieDown d ~ d2 } \\ 
      { g,2 ~ g } \\ 
      { \shiftOff s4. g'8 ~ g2 | } >> }
    { <f, f'>2 <g g'> }
  }
  << { a'8 h c d d e f g } \\ { a,4 c d f } >>
  <a, e' a>1 \bar "|."
}

dynamics =
{

}
