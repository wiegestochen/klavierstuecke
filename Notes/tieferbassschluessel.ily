
global = {
  \key e \major
  \numericTimeSignature
  \time 4/4
}

right = \relative c {
  \global
  \tempo 4=100
  %\tempo "souverän" 4=80
  \repeat volta 2 {
    \repeat percent 4 { fis16-2( h e,) } fis( h e, fis) |
    \repeat percent 4 { h( e, fis) } h( e, fis h) |
  }
  e,[( fis h]) dis,[( fis h]) h,( fis') \repeat percent 2 { h,[( cis e-4]) } e-1( fis) |
  e[( fis h]) dis,[( fis h]) h,( fis'-3) \repeat percent 2 { h,[( e h']) } h,( e) |
  
  %\tempo "neugierig"
  fis-3( gis e-1 fis) gis( e fis gis) \repeat percent 2 { a[( e fis]) } a( e) |
  fis( gis e fis) gis( e fis gis) \repeat percent 2 { a[( e fis]) } dis'-4( e) |
  
  %\tempo "ausgelassen"
  \repeat volta 2 {
    \repeat percent 2 { e\repeatTie( e, h'-2) } e( e,) h'( e e, h') e[( e, h']) e ~ |
  }
  %e e,[( h' e]) e,[( fis h e]) fis[( fis, h fis']) gis[( gis, h gis']) \bar "||" |
  \repeat percent 3 { e[( e, h']) } fis'[( fis, h]) gis'( gis, h gis') \bar "||" |
  %e( e, h' e) e,( h' e h) fis'[( fis, h]) gis'[( gis, h]) gis'( h,) \bar "||" |
  
  %\tempo "sehnsüchtig"
  \repeat percent 2 { gis( gis, cis) } \repeat percent 2 { e[( gis, cis]) } gis'( gis, cis e) |
  \repeat percent 2 { dis( fis, h) } fis'[( fis, h]) dis[( fis, h]) fis'( fis, h dis)
  \repeat percent 2 { cis( e, a) } e'[( e, a]) cis[( e, a]) cis( e, a cis) |
  \repeat percent 2 { h( e, a) } e'[( e, a]) h[( e, a]) h( e, a h) |
  
  %\tempo "glücklich"
  \repeat volta 2 {
    \repeat percent 4 { cis( e h) } cis( e h e) |
    \repeat percent 4 { d( e a,) } d( e h d) |
  }
  
  \time 2/4
  <h cis>4 \acciaccatura h8 <cis e>16( h a e) 
  \time 3/8
  <e a h>8 a16[( 
    \change Staff = "left" a,
    \change Staff = "right" e' 
    \change Staff = "left" cis]) 
    \change Staff = "right" \time 4/4
  <e fis a>4.( a8) a4( gis)\fermata \bar "|."
}

left = \relative c, {
  \global
  \clef "bass_8"
  %\ottava -1
  \repeat volta 2 {
    <e e,>1 |
    <cis cis,> |
  }
  <gis' gis,>2 <a a,>2 | 
  <gis gis,>2 <a a,>2 | 
  <e e,>2 <e e,> |
  <e e,> <e e,> |
  \repeat volta 2 {
    <a a,> <h h,> |
  }
  <e, e,>1 |
  <a a,> |
  <gis gis,> |
  <fis fis,> |
  <a a,> |
  \clef bass
  \repeat volta 2 {
    \repeat percent 4 { a16( e' a) } a,( e' gis e) |
    \repeat percent 4 { a,( d fis) } a,( e' gis e) |
  }
  
  << { a4 a }  \\ <a, e'>2 >> |
  <cis cis,>4. |
  <d a d,>2 e,8~ <e h'>~ <e h' e>4 |
  
}

dynamics =
{

}
