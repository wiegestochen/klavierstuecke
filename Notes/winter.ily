
PartPOneVoiceOne =  \relative des' {
  \clef "treble" \time 3/4 \key des \major % 1
  \tempo "Cantabile" 4=120 \partial 4 r8 des16-1 ( es | % 2
  f4 ges as | % 3
  as4 ges ) f ( | % 4
  es4 f ges | % 5
  ges4 f ) f8-3( es  | % 6
  des4 ges4.) f8( | % 7
  es4 as4.) ges8( | % 8
  ges4 f~ <f des~> | % 9
  <as, b des f>2->) f'8-1( ges \bar "||" 
  as4 as-2 b | % 11
  as2 ) 
  << { b8 ( c | % 12 
       des2. ~ -> | % 13
  des2 ) f,8-3 ges16 ( as ) | % 14
  b,4 b( as' ~ | % 15
  as2) as4 ~ | % 16
  as2 as4 ~ | % 17
  as2} \\ {
  s4 |
  \set fingeringOrientations = #'(up)
  <des-5>4 <as-3> ges | % 13
  f2 des4-1 | % 14
  as2. | % 15
  <b c>2. | % 16
  <c des>2. | % 17
  <des es>2
  } >> es8-1 ( f8 \bar "||"
  ges8 f-1) ges-2( des'-4 c des16 es | % 19
  as,8 ges ) as( es'-4 des es16 f | % 20
  b,8-1-- f'-3) 
  << {
    c-1--( f) des-1--( f ) | % 21
    des8_1( f ges f ges as ) \bar "||"
    <f as>4( <es ges> <des f> | % 23
    <c, es as, as'>2 )
  } \\ {
    s2 |
    es'4^2( des as) | 
    f'8( as,) es'( ges,) des'( f,) |
  } >>
  <b, b'>8 ( <c c'> | % 24
  <des ges as des>4 <as des es as> ) <ges as b ges'> ( | % 25
  <f as des f>2 ) <c' des f>4( | % 26
  <as c as' es>2. | % 27
  <ges b des>2.) | % 28
  <des' ges as b>2.( | % 29
  <as des es>2.) \bar "||"
  \time 6/8
  es'8-2( f es) f( es des | % 31
  as'2 ) r8 as ( | % 32
  as4 ges b, | % 33
  des4 c ) <b-1 ges'-5>8. f'16 | % 34
  <as, des>4( <a es'> <f'-4 c-1> | % 35
  <des-2 as'-5>4 <b-1 ges'-4>)\! 
  \override TextSpanner.bound-details.left.text = \markup { \italic\bold "rit." }
  <f'-5 a,-1>(\startTextSpan | % 36
  <es-3 des-2>4 ges-5 ~ -> <c,\finger \markup \tied-lyric #"1~4" ges'-5>  ~ | % 37
  <f, as c des>2\arpeggio\fermata)\stopTextSpan \bar "|."
}

PartPOneVoiceTwo =  \relative des {
  \clef "bass" \time 3/4 \key des \major % 1
  \partial 4 r4 | % 2
  des8 ( as' -3 des es f as, -5 | % 3
  b8 -4 ges' es c ) -3 des ( -1 as ) | % 4
  c,8 ( ges' -4 des' as b c | % 5
  des,8 as' des c, ) ~ <as' c,> ( ~ <c c,> ) | % 6
  b,8 ( ~ -> <b f'> ~ ~ -3 <b as' f>2 ) -> -1 | % 7
  c8 ( ~ -> <c ges'> ~ ~ <c c' ges>2 ) -> | % 8
  des8 ( ~ -> <des as'> ~ ~ <des as' des>2 ) -> | % 9
  ges,8 ( ~ <ges des'> ~ ~ <ges des' ges>4 ) r4
  << {
    ges8( des' ges des ges des) | % 11
    c8( as' es' des as es) | % 12
    f8-4( b-2 c-1 des-2) b-3( c-1 | % 13
    des,8 as' des ges,->-3) c( b | % 14
    ges,8 des' ges des) ges( des | % 15
    as8 es' as es) as( es | % 16
    b8 f' b f) b( f | % 17
    c8 as' c as) r4 \bar "||"
    es'2.-1 ~ | % 19
    es4 des2->-1 ~ | %20
    des2. s \bar "||"
  } \\ {
    ges,,2.| 
    c | % 12
    f2. | % 13
    des2. | % 14
    ges,2.\( | % 15
    as2. | % 16
    b2. | % 17
    c2.\) 
    r4 <es-5>->( <ges-4> | % 19
    c4 des) <f,-5>( | % 20
    ges4) as--( b -- | % 21
    <as,---3>4 b -- c ) -- 
  } >>
  <des des,>2. | % 23
  <ges ges,>2. | % 24
  <b,, b'>2 <c c'>4( | % 25
  <des des'>2) <b' f' b>4 ( | % 26
  <f des' f>2. | % 27
  <as es' as>2.) | % 28
  <f' b, b'>2.( | % 29
  <c ges' c>2.) \bar "||"
  <des, des'>2. | % 31
  \dynamicUp
  f8( des' f) ges( as4)\! | % 32
  << {
    ges,8 ( des'-3 as' es-3 ges as ) | % 33
    as,8 ( es'-2 b' ges-2) c,-4( a' | % 34
    b,8 g' ges c,-5 des-4 b'-1) | % 35
  } \\ {
    ges,2. |
    as |
  } >>
  a'8-2( h,-5 c des es f | % 36
  ges8-2 es-3 c-4 as) ges'-2( as | % 37
  <es-1 des-1 des,-5>2\arpeggio) \bar "|."
}

dynamics =
{
  s8 s8\mp | 
  s2.*18 |
  s4 s8\< s8 s8 s8 |
  s2. |
  
  s2.\f |
  s2 s4\mf |
  s2. |
  s2 s4\mp\> |
  s2.*3 |
  s2.\p |
  
  s2.\mp |
  s8\< s s s s4\! |
  s2.*2 |
  s2.\< |
  s4 s4\f s4\> |
  s2. |
  s2\p
}