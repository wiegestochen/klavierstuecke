
PartPOneVoiceOne =  \relative g' {
  \clef "treble" \stopStaff \override Staff.StaffSymbol #'line-count =
  #5 \startStaff \key g \major \numericTimeSignature\time 4/4 | % 1
   \tempo 4=120 
  g4-1^\markup { \italic "So mal eben" } h4 d-5 h | 
  a8 g a h a h a4 | 
  g h d h c8 a g fis-2 g4-1 r8 d'-5 |
  d h8 a g a h a d d h a g a4 r8 d d h a g a h a d d
  h8 a fis-1 g4-1 r g h d h a8 g a h a h a4 |
  g-2 <h-1 d,-4> d <h
  d,>4 c8 a g fis g2 \bar "|."
  \key d \major | % 13
  fis4-2\(^\markup { \italic "Never Ending Story" } a-4 h( a) |
  d, <fis d'> <e-3 a-5>2 \) h'4 ( g cis,4.fis8 d4 h <a g>2 ) h8 fis d' h cis
  g8 h cis d g, fis' d e ( fis g4 ) |
  fis8-4 c-1 e-3 fis g-1( a-4 h4-5) ~ |
  h2 <f' a,>4(\p <d f,> ) \bar "||"
  g,4.-3(\mf g8 a4.-4 a8 | %21
  h2-5 <a e> ) |
  s1 |
  d4 h a g fis -. a2. |
  h4-5 a g fis |
  ais,8-2 e' d4 g,8 e' g,4 |
  <fis d'>1 \bar "|." |
  \key g \major | % 29
  g'4-1( h-3) d( h) |
  c8( h a g) fis2-3 |
  d4-2 e8. fis16 g4-1 a8. h16 |
  c4 a fis2 |
  g4( h) d( h) |
  c8.-4 h16 a8. g16 fis2-2 |
  h8 d,8 a' g c, a'-5 ~ <a d,_2> ~ <fis-4 d> ~ |
  <h, d g>1 \bar "||"
  <g'-1 d'-2>4( <h-1 g'-4>) <d-2 h'-5>( <h g'>) |
  g'8-5 fis e fis g4 d-2 |
  e8-3 d c e d4 a'-5 |
  h-4 d-5 <a a,>2 <d,-2 h'-5>4 <h-1 g'-4> <g-1 e'-3> <h-1 g'-4> |
  <h-1 g'-4> <cis a'>8 <g' h,> <a,-1 fis'-3>4 <a d> |
  c-4 d8 c h4 c8 h |
  a4-5( \parenthesize fis-4) g2-5 \bar "||" |
  \time 2/4  g8-1 h-2 d-4 h |
  c-3 a-1 fis-4 c-2 |
  h d g-4 a |
  fis4-3 c |
  g'-4 e8-2 g |
  d4-1 a'-2 |
  h-3 d-5 |
  a2-1 \bar "||"
  \numericTimeSignature\time 4/4  h8-2 a h c \once\slurDown d4( ~ <d \parenthesize a>) |
  e8-4 d <c e> <fis a,> \once\slurDown <g-5 h,-2>4( ~ <g h, \parenthesize g>) |
  g8-5 fis e d r <c-4 a^2> h c <a-3 d-5>2 h <d, h'>4 <d' g,>8 <h
  d,>8 <a c,>4 d,8 c <d g>4 <fis a> c'( h) <h g> a8( h) e4( d c
  h4 a2) \bar "||"
  h8( a g fis c2) a'8( g fis c h2) a4( h8 c d4 d) e e fis fis g2
  fis8 h g e fis4 h, e <a, e'> <fis dis'>1 \bar "||"
  \key c \major <h e>4 h'8 e, <c e c'>4 e8 f? <h, g'>4 h'8 e, <a a,>2
  <c, e>4 e8 g <d g>2 e4 g ~ <g d>4. f8 ~ <a, f'>4 h <c e>2 d4 g e2
  fis4 h g2 g <fis c'> \bar "||"
  \key g \major <d g h>4 h'8 c d4 a g g8 c h2 e4 e8 fis g d h
  g8 a h a h c2 | \barNumberCheck #80
  \key c \major <d, g h>4 h'8 c d4 g, d' e8 h c2 d,4 e8 f a ( g4
  e8 ) ais ( a4 ) f8 h4. d8 c4 a e f g2 <g d> <c g e>1 \bar "|."
}

PartPOneVoiceThree =  \relative e {
  \clef "bass" \stopStaff \override Staff.StaffSymbol #'line-count =
  #5 \startStaff \key g \major \numericTimeSignature\time 4/4 s1*12
  \bar "||"
  \key d \major s1*8 \bar "||"
  e'8^> cis h a fis'^> d cis a |
  s1*5 r2 e-3 s1 \bar "||"
  \key g \major s1*8 \bar "||"
  s1*3 s2 d'4( c) s1*4 \bar "||"
  \time 2/4  s1*4 \bar "||"
  \numericTimeSignature\time 4/4  s1*4 |
  r8 d,4. r8 d4. s1*3 \bar "||"
  s1*7 \bar "||"
  \key c \major s1*3 |
  c'8 g r4 r8 d4. |
  s1 |
  s2. e,8 f s2. h8 c h'2( a) \bar "||" |
  \key g \major s1*4 | \barNumberCheck #80
  \key c \major s1*7 \bar "|."
}

PartPOneVoiceTwo =  \relative g' {
  | % 1
  s1*12 \bar "||"
  \key d \major | % 13
  s2 g s d8-2 ( h cis4 ) s2 g
  s1*4 f'4( d) s2 \bar "||"
  d2 e <fis d>4 fis8 d d4( cis) ~ <a cis fis>1 |
  r8 g'4 fis e e8 ~ e4 r <a, h d>2 s1 s1 s1 \bar
  "||"
  \key g \major | % 29
  s1 
  s2. c4-1 | 
  s1*6 \bar "||"
  s1 c'2-2 h r a s1*3 fis2 d c h \bar "||"
  \time 2/4  s1*4 \bar "||"
  \numericTimeSignature\time 4/4  s1 |
  s1 |
  d'4-2 g, r8 r g4 g-2(
  fis4) g2 s1 r2 d s1*2 \bar "||"
  s1*7 \bar "||"
  \key c \major s1*2 r2 c4 h8 g' r4. c,8 ~ c4 h s1*4 \bar "||"
  \key g \major s1*4 | \barNumberCheck #80
  \key c \major s1 r2 r8 e4. s2 d c f8 e4. s1*3 \bar "|."
}

PartPOneVoiceFive =  \relative g {
  \clef "bass" \key g \major \numericTimeSignature\time 4/4 
  g8-5 d'-1 h d g, d' h d |
  <d, c'>1 |
  g8 d' h d g, d' h d |
  <d, fis c'>4 <fis c' d>4 g-2 r |
  g-2 c fis,-3 c' |
  g c fis,-3 d-5 |
  g c fis, c' |
  g <c fis,>4 <g h> r |
  g8-5 d' h d g, d' h d |
  <d, c'>1 |
  <d-4 h'-1>4 <g-2 h,-5>4 <g-2 h-1> <g-2 h,-5> |
  <d fis c'> <fis c' d> h8 d,-3 g,4 \bar "|." |
  \key d \major d'8 a'-2 fis-3 d' e, cis'-2 a-3 e' |
  h,8 fis'-3 h cis a,2 |
  g8 d'-3 e h' a, e'-2 cis4-3 |
  h8-4 fis'4-1 d8 cis e a,-4 g |
  <g d'>4 ( g' ) <e a> ( a, ) <h-4 fis'-2> ( h' <cis,-5 a'-2> cis' )
  d,4 a'-1 h, f'-1 |
  g,8 d'-3 f h d2^> \bar "||" |
  e,2 fis |
  h8 fis-3 d g-2 a e a, cis |
  d, a'8-3 d e fis2 |
  <h e, d'>2 <cis a cis,> |
  d,8 a'-3 cis d g,,8 ~ <g d'> ~ <g d' fis> ~ <g d' g> |
  cis-5 g' d fis e cis' d h |
  cis,-3 fis h, a'-1 e cis a' a,-3 ~ <d, d' a>1 \bar "|."
  \key g \major h''2-1 g-2 |
  a-1 d,4 r |
  h'-1( g-2 e-3 c?-5 |
  fis-4 c' d d,) |
  h'2-1 g-3 |
  a2-2 d,8 c' a fis-4 |
  g4-3 c a-2 d,-2 ~ |
  <g, d' g>1 \bar "||"
  h'2-3 g-5 |
  a4-4 c-2 g-5 f'-1 |
  c a fis-5 d'-1 |
  g,-4 h-2 fis2 |
  g4-1 e8-2 h-5 c4 d |
  cis e4 d8 e fis4-1 |
  a4.-2 a8 g4.-3 g8 |
  d4-1( d,-5) g2-2 \bar "||"
  \time 2/4  h'4-1 g-2 |
  fis-3 a-1 |
  g d-5 |
  a'8 d, fis a |
  e-2 h-5 c h |
  g' fis c-5 d |
  g d h' g |
  fis d c' a \bar "||" |
  \numericTimeSignature\time 4/4  g,4 d'-3 \once\slurUp fis( ~ <fis \parenthesize c'>) fis-5 a \once\slurUp g( ~ <g \parenthesize d'>) |
  h-2 c?4 e2 |
  <a, d> <g d'> |
  g fis h8( d, fis d) <g g,>2 g8( d c' d c
  fis,8 g h fis a g d' fis,4) d8( c) \bar "||"
  g'4( h) a8( g fis e) d4( a') g8( fis e d) c fis g a h g fis g
  c8 a g a d c a c e h g fis d4 h' h,2 c <h' h,> <a, a'>
  \bar "||"
  \key c \major <g g'>2 <a' a,> <e e,> <f f,> <a a,> <g g,> |
  c,2 <g' g,>2 d4 g, a e <h' g'>2 <g' c,> <d a'> <e h'> |
  d1 \bar "||" |
  \key g \major <g, d' g>2 fis'8 d' c d, e h4. g'8 fis e d c
  g'8 c4^> h d fis, c' fis,4( d) | \barNumberCheck #80
  \key c \major g,8 h' d4 f,8 f'4 h,8 e, h'4 d8 a,4 a'8 e f f,4 d'8
  h4 c f e gis h, <c a'>2 <f c'> <c' g> ~ <h g> ~ <c, c' g>1 \bar "|."
}

dynamics =
{

}
