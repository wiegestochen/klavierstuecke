
global = {
	\tempo 4 = 140
	\key fis \minor
	\time 6/8
}

upper = \relative c' {
     \global
     \partial 4
     \slurUp
     gis'4 
     a4( d, ~ <d a'>~  |
     <d gis>2)\fermata d'4 |
     <gis,~ cis>4( \once \tieDown <cis, gis'>~ <cis~ gis' cis> |
     <cis f cis'>4.)\fermata cis8 fis gis |
     <d fis a>4( d8) <fis a>4( d8) |
     <fis gis>4( d8) <fis gis d'>4( d8)
     <cis gis' cis>4( cis8) cis'4( cis,8)
     <fis cis'>4\( cis ~ <cis f cis'>\)
     \bar "||"
     \phrasingSlurUp
     <d fis d'>4(\( d8) <fis d'>4( d8)
     <fis h>4( d8) <fis h>4( d8)
     <fis a>4( cis8) <fis a>4( cis8)
     <fis gis>4( cis <f gis>)\)
     %\tempo 4 = 160 %ungeduldig
     \repeat volta 2 {
     	     <fis a>4( cis8) <fis a>8( cis4)
     	     <fis cis'>4( cis8) <fis a>4( cis8)
     	     <fis gis>4( cis8) <fis gis>8( cis4)
     }
     \alternative {
     	     { <gis' h>4\( cis,8 gis'4 ~ <cis, gis'>8\) } %<< {gis'4.} \\ {s4 cis,8} >> }
     	     { <gis' h>4\( cis, gis'\) }
     }
     \repeat volta 2 {
	<fis cis' fis>4 fis8[( cis' fis fis,])
	<fis cis' fis>4 fis8[( cis' fis fis,])
	<fis cis' fis>4( fis8) <cis' fis>4( fis,8)
	<fis cis' fis>4 ~ <gis cis gis'> <f cis' f>
     }
     \time 3/8
     <fis cis' fis>8( fis cis')
     fis( fis, cis')
     fis( fis, cis')
     fis( fis, cis')
     <a cis a'>4. ~ <a cis a'>4. 
     <gis h gis'>4. ~ <gis h gis'>4.
     \time 6/8 %\tempo 4 = 180
     \repeat volta 2 {
     	     cis8 cis, fis cis' cis, fis 
     	     cis' cis, fis cis' cis, fis
     	     cis' cis, gis' cis cis, gis' 
     	     cis cis, a' cis cis, a'
     	     cis cis, a' cis cis, a' 
     }
     fis8\( gis a fis gis a
     gis8 a h gis a h
     a8 h cis a h cis
     h[ cis d cis a e]\)
     fis8\( gis a fis gis a
     a4 h gis\)
     a8\( h cis a h cis
     h[ cis d e cis a]\)
     fis'\( fis, cis' fis fis, cis'
     gis' gis, cis gis' gis, cis
     a' a, cis a' a, cis
     %\set Timing.beatStructure = #'(4 1 1)
     h'[ h, e] h'[\) h,] \breathe d'\(
     %\time 6/8
     \bar "||"
     cis4 a cis,
     d h'4.\) a8\(
     h4 gis e 
     cis a'4.\) gis8\(
     fis4 d h
     e cis a
     d h gis
     <cis, gis' a>2\) gis'4
     <cis, fis>( gis' a
     %<d, a' cis>4 << {s4. d8} \\ \\ {<gis h>2} >>
     <d, a' cis>4 <gis h>4.) d8 |
     <cis fis>4( gis' a
     \time 4/4
     <d, gis cis>2 h'2)
     \bar "||"
     <cis a' cis>2 <h gis' h>~ q <gis e' gis>4 <a fis' a>
     <cis a' cis>2 <h gis' h>~ q <gis e' gis>4 <a fis' a>

     <fis d' fis>( <e cis' e>) <cis a' cis>(
     <d h' d>) <cis a' cis>( <h gis' h>)
     <gis e' gis>( <a fis' a>) <h cis e h'>(
     <cis cis'> <e e'> <gis gis'>) \bar "||"
     <a a'>2( h8 a gis fis) 
     gis( a gis4 fis f) 
     fis( cis'4) r8 fis,( f dis)
     f( fis f4 h gis)
     a2( h8 a gis fis
     %\times 4/3 
     { gis4 cis2) gis4( }
     fis4) h8( a h4) fis8( gis)
     f( gis) dis( gis) <cis, fis>4( gis') \bar "||"
     \time 6/8
     a8\( gis fis gis a h
     cis d cis h cis d
     cis a cis h cis d
     e cis e f cis gis'\)
     a\( gis fis f fis gis 
     fis cis fis d e d
     cis fis cis f fis gis 
     fis cis\) r4 fis,8 gis
     <fis a>( <gis h> <fis a> <f gis> <fis a> <gis h>)
     <a cis>( <h d> <a cis> <gis h> <a cis> <h d>)
     <a cis>( <fis a> <a cis> <h d> <cis e> <d fis>)
     <cis e>( <a cis> <cis e>) <d f> <f h> <gis cis>
     <fis a>( <gis h> <a cis> <f gis> <fis a> <gis h>)
     <fis a>( <e gis> <d fis> <h f'> d cis)
     a f'( fis) h, f'( fis)
     cis fis( gis) d a'( gis) \bar "||"
     h( h, d g g, h)
     a( c g' fis e d)
     d( d, fis h h, d)
     a( c e a h c) |
     
     \tempo 4 = 110
     <fis, d'>4( <e cis'> <e a> <a, d>) | % 94
     <fis' d'>8( <e cis'> <a e'> <fis d'> <e a>4 <a, d>)
     h8( d fis g <h, d a'>4.) a8
     h( d fis h <h, d e a>4) a'8 h \bar "||"
     <fis a cis>4\( <f a h> <e h' d> <e a cis>
     <e a e'>4 <a, d a'> <e' a e'> <gis, d' gis>8( d'')\) \bar "||"
     <cis, e cis'>4 <cis e cis'> <e a e'> r16 e a e'
     <fis, a fis'>4 <e a e'> <gis h gis'> <a cis a'>~
     <a cis a'> <gis d' fis>8\( <a cis e> <fis~ h d> <fis a cis> <e~ a h~>4
     <e gis h> <e a d> <a d a'>~\fermata\) a'8 gis 
     fis cis fis, cis a2\(
     h4 cis8 d cis2\) | 
     \time 6/8
     << { d8\( e fis e h d} \\ {a4. h} >> |
     << { h4.( a8)\) cis\([ e] } \\ <cis e>4. >> | \bar "||"
     << { fis8 gis a e gis a | <a, d>2\) a'8\( h } \\ { cis,4. h } >> |
     %<a d>2 a'8 h |
     << { cis'8 h a <e a h e>8 e a <a, d e a>2\) } \\ { <fis' a>4. } >>  a8\( h |
     < fis a cis >4. < e a > | 
     < a, d fis > < a d a' >\)\fermata | 
     a'8\( d a' \ottava #1 a d a' d a d, \ottava #0 a d, a\) |
     <g, cis>\( e' a e' a cis |
     \ottava #1 e' cis a g e cis |
     <a d a'>2.\)\fermata |
}

lower = \relative c {
     \global
     \partial 4
     s4
     cis'2.
     h
     a
     gis2.
     %\bar "||" %\time 6/8 \partial 4.
     %s4.
     cis2 cis4
     h2 h4
     a2 a4
     gis2.
     %\key h \minor \bar "||" 
     g2 g4
     g2 g4
     a2 a4
     cis,2.
     %\tempo 4 = 160
     \repeat volta 2 {
     	     a'2 r8 a | 
     	     a2 a4
     	     h2 r8 h | 
     }
     \alternative {
     	     { h2 h4 }
     	     { h2. }
     }
     \repeat volta 2 {
	<fis fis,>2.
	<e e,>
	<d d,>2 d4
	<cis cis,>2.
     }
     \time 3/8
     <fis fis,>4.
     <f f,>
     <e e,>
     <dis dis,>
     <d d,>4. ~ <d d,>4.
     <f f,>4. ~ <f f,>4.
     \time 6/8 %\tempo 4 = 200
     \repeat volta 2 {
     	     d4. d'
     	     d, d'
     	     e, e'
     	     fis, fis'
     	     fis, fis' 
     }
     << { d4. d } \\ <d, a'>2. >> |
     << { e'4. e } \\ <e, h'>2. >> | 
     << { fis'4. fis } \\ <fis, cis'>2. >> |
     <gis e' gis>4. <a e' a>
     << { d4. d } \\ <d, a'>2. >> |
     <e h' e>2 e'4
     << { fis4. fis } \\ <fis, cis'>2. >> |
     <gis e' gis>4. <a e' a>
     << { d4. d } \\ <d, a'>2. >> %<d,~ a'~ d>4. <d a' d>%d'
     << { e'4. e } \\ <e, h'>2. >> %<e~ h'~ e>4. <e h' e>%e'
     << { fis'4. fis } \\ <fis, cis'>2. >> %<fis~ cis'~ fis>4. <fis cis' fis>%fis'
     << { gis'4. gis } \\ <gis, e'>2. >> %<gis~ e'~ gis>4. <gis e' gis>%gis'
     \ottava #1
     %\clef treble
     a8( e' a cis h a)
     fis,( d' fis h a fis)
     gis,( e' gis d' h gis)
     a,( e' a cis h cis)
     fis,,( d' fis a gis fis)
     e,( cis' e gis fis e )
     e,( gis h d f, d' 
     \ottava #0
     %\clef bass
     %<< { \voiceOne fis,,2. } \new Voice { \oneVoice \once \override NoteHead #'transparent = ##t fis4 fis' s } >> 
     <fis,, fis'>2.)
     <d' a' d>2.
     <e h' e>
     <d a' d>2.
     \time 4/4
     <e h' e>1
     <d a' d>1 ~
     <d a' d>1
     <d a' d>1 ~
     <d a' d>1

     <d a' d>1
     %<d a' d>
     << { \voiceOne d'2 d } \new Voice { \voiceThree <d, a'>1 } >>
     <a e' a>
     cis'1\( 
     h
     a
     gis\)
     << { \voiceOne fis'2\( fis } \new Voice { \voiceThree <fis, cis'>1 } >>
     << { \voiceOne f'2 f } \new Voice { \voiceThree <f, cis'>1 } >>
     << { \voiceOne d'2 d\) } \new Voice { \voiceThree <d, a'>1 } >>
     << {cis'2( h)} \\ {<cis, gis'>1} >>
     \time 6/8 \clef treble
     cis'4.\( d
     e f
     fis gis
     a h\)
     cis\( h
     a h
     a gis
     fis4\) \clef bass <cis, cis'>8 <fis, fis'>4.
     \clef treble cis''4.\( d
     e f
     fis gis
     a h\)
     cis\( h
     a gis\)
     <fis, cis' fis>4.\(
     <gis d' gis>
     <a fis' a>
     <h fis' h>\) 
     <g d' g>2.
     <g d' g> \clef bass
     <g, d' g>
     <g d' g>
     \time 4/4
     <g d' g>2 <fis d' fis>
     %g8~ <g d'>~ <g d' a'>4 << fis2 \\ { \voiceOne s8 d'4. } \\ { \voiceOne s4 fis4 } >>
     << { g2 fis } \\ { \voiceTwo s8 d'8~ <d a'>4 s8 d~ <d fis>4 } >>
     g,8 d' g r a,8 e' a r
     g,8 d' g d a e' r4
     cis'4 h a gis
     <f, f'>2 
     <e e'>2
     <a e' a>4 <a e' a>4
     << {cis'4 cis} \\ {<cis, a'>2} >>
     <d a' d>4 <cis a' cis> <e, e'> <fis fis'>~
     <fis fis'> <d' d'>8 <cis cis'> <d, d'>4 <e e'>~
     <e e'> <fis fis'>2.
     <d' a' d>2 <fis, cis' fis>
     <gis d' gis> <a e' a> |
     <fis d' fis>4. <gis e' gis> |
     <a e'>2. |
     <d a' d>4. <cis a' cis> |
     << {h'4. h} \\ {<h, fis'>2.} >> |
     <d a' d>4. <cis a' cis> |
     << {h'4. h} \\ {<h, fis'>2.} >> |
     <d a' d>4. <cis a' cis> <c g' c> <ais f' ais>~ |
     <ais f' ais>2.~ |
     <ais f' ais> |
     <a! e' a!>~ |
     <a! e' a!> |
     \ottava #-1 <d, d,> \bar "|."
}

dynamics = {
     
	%s2\fff\> s4
     %s\!\pp
}

pedal = {
     %\sustainOn s2\sustainOff
}

