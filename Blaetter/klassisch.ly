
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/klassisch.ily"

\header {
  title = "Albumblatt Nr. 1"
  subtitle = "“Klassisch”"
  composer = "Hendrik Reupke"
  tagline = ##f
  copyright = \markup{ 
    \with-url #"https://creativecommons.org/licenses/by/4.0/" {
      \center-column{ 
        \epsfile #X #10 #"../Copyright/by.eps" 
      }
    }
  }
}

\paper {
  #(set-paper-size "a4")
  page-count = #1
}

global = {
  \key c \major
  \time 4/4
}

\score {
  \new PianoStaff \with {
    instrumentName = "Klavier"
  } <<
    \new Staff = "right" \right
    \new Dynamics = "dynamics" \dynamics
    \new Staff = "left" { \clef bass \left }
  >>
  \layout { }
}

\score {
  \articulate \new PianoStaff <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } << \right \dynamics >>
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } << { \clef bass \left } \dynamics >>
  >>
  \midi {
    \context {
      \Staff
      \consists "Dynamic_performer"
    }
  }
}
