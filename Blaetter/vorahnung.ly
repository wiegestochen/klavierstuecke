
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/vorahnung.ily"

\header {
  title = "Vorahnung"
  instrument = "Klavier"
  composer = "Hendrik Reupke (2011)"
  %copyright = "© Hendrik Reupke"
}

\paper {
  #(set-paper-size "a4")
}


\score {
  \new PianoStaff \with {
    instrumentName = ""
  } <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } << \rightOne \\ \rightTwo >>
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \left }
  >>
  \layout { }
}


\score {
  \articulate \unfoldRepeats \new PianoStaff <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } << \rightOne \\ \rightTwo >>
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \left }
  >>
  \midi {
    \tempo 4=100
  }
}
