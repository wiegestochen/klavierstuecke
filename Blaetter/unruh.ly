
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/unruh.ily"

\header {
  title = Unruh
  subtitle = "Tick - Tick - Tick"
  composer = "Hendrik Reupke"
  poet = "12. - 17. Juni 2021"
  tagline = ##f
  copyright = \markup{ 
    \with-url #"https://creativecommons.org/licenses/by/4.0/" {
      \center-column{ 
        \epsfile #X #10 #"../Copyright/by.eps" 
      }
    }
  }
}

\paper {
  page-count = 1
}

% The score definition
\score {
  <<
    \new PianoStaff <<
      \set PianoStaff.instrumentName = "Klavier"
      %\set PianoStaff.shortInstrumentName = "Klav."
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
      >> \context Staff = "2" <<
        \context Voice = "PartPOneVoiceTwo" { \PartPOneVoiceTwo }
      >>
    >>

  >>
  \layout {}
}

\score {
  \articulate \unfoldRepeats <<
    \new PianoStaff <<
      \set PianoStaff.instrumentName = "Klavier"
      %\set PianoStaff.shortInstrumentName = "Klav."
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceThree" { \voiceTwo \PartPOneVoiceThree }
      >> \context Staff = "2" <<
        \context Voice = "PartPOneVoiceTwo" { \PartPOneVoiceTwo }
      >>
    >>

  >>
  \midi {}
}

