
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"

\header {
  title = "Albumblatt"
  subtitle = "Sympathetic Resonance"
  subsubtitle = "(an Manfred und Moni denkend)"
  composer = "Hendrik Reupke"
  poet = "13. Juni 2020"
  %meter = "Legato"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

global = {
  \key d \major
  \time 12/8
  \tempo "Legato" 4=80
}

right = \relative c'' {
  \global
  << { g4.( a2. ~ a4 fis8-> ~ 1.) } \\ { d4.( e4. <h e>2. <a h>1.) } >> |
  <g' d>4(\( <a e>) <h e,>2 <a e a,> | 
  <d a fis e>2\) << { g,4( fis) } \\ { <d g,>2( <e h g>2 | 
  <d fis,>1)^\fermata } >>
  \arpeggioArrowDown 
  <g, e>4( <a fis>) | 
  << { \slurDown s4 s4. g'16( e4.) a16( d,4.) s4 s4. d'16( h4.) e16( a,4.) } \\ { <d, h g>1. ~ <d h g> } >> |
}

left = \relative c {
  \global
  <e h' e>4.( <fis d' fis> <cis a' cis>2. |
  <g d' g>1.) |
  <e' h' e>4(\( <fis d' fis>) <g cis g'>2 <cis, g' cis> |
  <fis, d' fis>2\) <h e h'>4( ~ <h fis' h> 
  <a d a'>4 ~ < a cis a'> 
  \ottava #-1
  <d, d,>1)\fermata 
  <e, e'>4( <fis fis'>) | <g g'>1. ~ <g g'> 
  \ottava #0
}

\score {
  \new PianoStaff \with {
    instrumentName = "Klavier"
  } <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } { <>^"Rubato" \right }
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass <>_\pedadlib \left }
  >>
  \layout { }
  \midi { }
}
