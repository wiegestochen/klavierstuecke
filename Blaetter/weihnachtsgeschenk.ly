
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/weihnachtsgeschenk.ily"

\header {
  title = "Albumblatt Nr. 3"
  subtitle = "Weihnachtsgeschenk"
  dedication = "für Ally"
  composer = "Hendrik Reupke (2002)"
  tagline = ##f
  %datum = "Weihnachten 2002"
  %instrument = "Klavier"
  copyright = \markup {\with-url #"https://creativecommons.org/licenses/by-sa/3.0/de/" "(CC-BY-SA)"}
}

\paper {
  %page-count = #1
}


\score {
  \new PianoStaff << 
    \set PianoStaff.instrumentName = #"Klavier"
    \new Staff = "up" { \key d \major \melody }
    \new Staff = "down" { \clef bass \key d \major \base }
    \new Dynamics \with { pedalSustainStyle = #'mixed } \pedal
  >>
  \layout {}
}

\score {
  \articulate \new PianoStaff << 
    \new Staff << \melody >>
    \new Staff << \base >>
    \new Staff << \pedal_midi >>
  >>
  \midi {
    \context {
      \Staff
      \consists "Dynamic_performer"
    }
  }
}
