
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/never_ending_story.ily"
\include "tabletpaper.ly"

\header {
  copyright = "https://creativecommons.org/licenses/by-nc-nd/4.0/"
  title = "Never Ending Story"
  composer = \markup \right-column { "\"So mal eben\" von Tanja Schmidt-Soltau" 
                                     "Hendrik Reupke" }
  poet = "Mai 2021"
  copyright = \markup{ 
    \with-url #"https://creativecommons.org/licenses/by-nc-nd/4.0/" {
      \center-column{ 
        \epsfile #X #15 #"../Copyright/by-nc-nd.eu.eps" 
        % "© Tanja Schmidt-Soltau und Hendrik Reupke (2021)"
        % "Dieses Werk ist lizenziert unter der Creative Commons Lizenz:" 
        % "Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International"
      }
    }
  }
  tagline = ##f
}

\paper {
  %page-count = 2
}

\tabletpaperquer


% The score definition
\score {
  <<
    \new PianoStaff <<
      \set PianoStaff.instrumentName = "Piano"
      %\set PianoStaff.shortInstrumentName = "Pno."
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceTwo" { \voiceTwo \PartPOneVoiceTwo }
      >> \context Staff = "2" <<
        \context Voice = "PartPOneVoiceThree" { \voiceOne \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceFive" { \voiceTwo \PartPOneVoiceFive }
      >>
    >>

  >>
  \layout {}
}

\score {
    \articulate \new PianoStaff <<
      \context Staff = "1" <<
        \context Voice = "PartPOneVoiceOne" { \voiceOne \PartPOneVoiceOne }
        \context Voice = "PartPOneVoiceTwo" { \voiceTwo \PartPOneVoiceTwo }
      >> \context Staff = "2" <<
        \context Voice = "PartPOneVoiceThree" { \voiceOne \PartPOneVoiceThree }
        \context Voice = "PartPOneVoiceFive" { \voiceTwo \PartPOneVoiceFive }
      >>
    >>
  \midi {}
}

