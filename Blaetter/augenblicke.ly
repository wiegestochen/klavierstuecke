
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/augenblicke.ily"
\include "tabletpaper.ly"



\header {
	dedication = "Für Mutti"
	title = "Albumblatt Nr. 2"
	subtitle = "“Augenblicke von Dauer”"
	composer = "Hendrik Reupke"
	%datum = "Weihnachten 2010"
	%instrument = "Klavier"
	copyright = "(CC-BY-SA)"
}

\tabletpaperquer

\score {
     \new PianoStaff \with { instrumentName = "Klavier" } <<
     \new Staff = "upper" \upper
     %\new Dynamics = "dynamics" \dynamics
     \new Staff = "lower" <<
     \clef bass
     \lower
     >>
     %\new Dynamics = "pedal" \pedal
     >>
     \layout {
	     \context {
		     \type "Engraver_group"
		     \name Dynamics
		     \alias Voice % So that \cresc works, for example.
		     \consists "Output_property_engraver"
		     
		     \override VerticalAxisGroup #'minimum-Y-extent = #'(-1 . 1)
		     pedalSustainStrings = #'("Ped." "*Ped." "*")
		     pedalUnaCordaStrings = #'("una corda" "" "tre corde")
		     
		     \consists "Piano_pedal_engraver"
		     \consists "Script_engraver"
		     \consists "Dynamic_engraver"
		     \consists "Text_engraver"
		     
		     \override TextScript #'font-size = #2
		     \override TextScript #'font-shape = #'italic
		     \override DynamicText #'extra-offset = #'(0 . 2.5)
		     \override Hairpin #'extra-offset = #'(0 . 2.5)
		     
		     \consists "Skip_event_swallow_translator"
		     
		     \consists "Axis_group_engraver"
	     }
	     \context {
		     \PianoStaff
		     \accepts Dynamics
		     \override VerticalAlignment #'forced-distance = #7
	     }
     }
}


\score {
  \unfoldRepeats \articulate \new PianoStaff <<
     \new Staff = "upper" << \upper \dynamics >>
     \new Staff = "lower" << \clef bass \lower \dynamics >>
     \new Dynamics = "pedal" \pedal
     >>
     \midi {
	     \context {
		     \type "Performer_group"
		     \name Dynamics
		     \consists "Piano_pedal_performer"
	     }
	     \context {
		     \PianoStaff
		     \accepts Dynamics
	     }
     }
}


