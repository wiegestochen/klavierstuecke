
\version "2.23.6"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/abschied.ily"

\header {
  dedication = "für Katrin General, zum Abschied"
  title = "Abschied"
  composer = "Hendrik Reupke"
  poet = "10. – 12. März 2022"
  %tagline = ##f
  copyright = \markup{ 
    \with-url #"https://creativecommons.org/licenses/by/4.0/" {
      \center-column{ 
        \epsfile #X #10 #"../Copyright/by.eps" 
      }
    }
  }
}


\score {
  %\void \displayMusic \piano
  \new PianoStaff  <<
    \set PianoStaff.instrumentName = "Klavier"

    \new Staff = "upper" \right
    \new Dynamics = "dynamics" \dynamicsLayout
    \new Staff = "lower" { \clef "bass" \left }
  >>
  \layout {}
  %\midi {}
}

\score {
  \articulate \new PianoStaff <<
    \new Staff = "upper" << {
      \global s1 
      \rightCaput \rightCaputEnd 
      \rightInterlude
      \rightCaput \rightCoda \rightCodaEndeMidi
    } \dynamicsMidi >>
    \new Staff = "lower" << {
      \global s1 \clef bass \global 
      \leftCaput \leftCaputEnd 
      \leftInterlude
      \leftCaput \leftCoda \leftCodaEndeMidi
    } \dynamicsMidi >>
  >>
  %\layout {}
  \midi {
    \context {
      \Staff
      \consists "Dynamic_performer"
    }
  }
}

