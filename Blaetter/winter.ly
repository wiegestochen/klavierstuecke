
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/winter.ily"

\header {
  %title = Winter
  title = "Happy New Year"
  subsubtitle = \markup \center-column { "Inspiriert von den Weihnachstliedern" "''The First Noël'' und ''We Wish You a Merry Christmas''" }
  composer = "Hendrik Reupke"
  poet = "Mai 2021"
  %tagline = ##f
  copyright = \markup{ 
    \with-url #"https://creativecommons.org/licenses/by/4.0/" {
      \center-column{ 
        \epsfile #X #10 #"../Copyright/by.eps" 
      }
    }
  }
}

\paper {
  page-count = 1
}

\score {
  %\void \displayMusic \piano
  \new PianoStaff  <<
    \set PianoStaff.instrumentName = "Klavier"

    \new Staff = "upper" \PartPOneVoiceOne
    \new Dynamics = "dynamics" \dynamics
    \new Staff = "lower" \PartPOneVoiceTwo
  >>
  \layout {}
}

\score {
  \articulate \new PianoStaff <<
    \new Staff = "upper" << \PartPOneVoiceOne \dynamics >>
    \new Staff = "lower" << \PartPOneVoiceTwo \dynamics >>
  >>
  \midi {
    \context {
      \Staff
      \consists "Dynamic_performer"
    }
  }
}

