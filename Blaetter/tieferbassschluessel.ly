
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/tieferbassschluessel.ily"
\include "tabletpaper.ly"

\header {
  title = "Albumblatt Nr. 4"
  %subtitle = "Pubertät"
  subtitle = "Tiefer Bassschlüssel"
  composer = "Hendrik Reupke (ca. 1997)"
  copyright = "© Hendrik Reupke (CC-BY-SA)"
}

\paper {
  #(set-paper-size "a4")
}

\layout {
  \context {
    \Voice
    \consists "Melody_engraver"
    \override Stem #'neutral-direction = #'()
  }
}

\tabletpaperquer

\score {
  \new PianoStaff \with {
    instrumentName = "Klavier"
  } <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \right }
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \left }
  >>
  \layout { }
}

\score {
  \articulate \unfoldRepeats \new PianoStaff \with {
    instrumentName = "Klavier"
  } <<
    \new Staff = "right" \with {
      midiInstrument = "acoustic grand"
    } { \clef bass \right }
    \new Staff = "left" \with {
      midiInstrument = "acoustic grand"
    } { \left }
  >>
  \midi { }
}
