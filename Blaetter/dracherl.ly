
\version "2.22.0"
\language "deutsch"

\include "articulate.ly"
\include "../albumDefs.ily"
\include "../Notes/dracherl.ily"

\header {
  dedication = "Für mein Dracherl"
  title = "Albumblatt"
  subtitle = "“Liebeslied”"
  composer = "2006"
  %composer = "Hendrik Reupke"
  subsubtitle = ##f
  copyright = "(CC-BY-SA)"
}

\score {
  \new PianoStaff \with {
      instrumentName = \markup { \fontsize #4 \bold "#." }
    } <<
      \new Staff = "right" \right
      \new Staff = "left" { \clef "bass" \left }
    >>
  \layout {}
}

\score {
  \articulate \new PianoStaff <<
      \new Staff = "right" \right
      \new Staff = "left" { \clef "bass" \left }
    >>
  \midi{}
}